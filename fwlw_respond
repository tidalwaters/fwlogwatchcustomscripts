#!/bin/bash
# Copyright (C) 2000-2010 Boris Wesslowski
# $Id: fwlw_respond 706 2010-10-06 12:02:36Z bw $
# fwlogwatch realtime response script

# Set the $MODE variable to activate realtime modification of
# ipchains or netfilter packet filters.

# You may want to add custom commands at the commented spots to modify
# tcp wrappers or ipfilter rules or even remote control access lists
# on cisco routers...

# $TARGET contains the name of the chain that will be used for rules
# generated by this script.

# See fwlw_notify for the contents of the variables passed by fwlogwatch
# The available arguments (if activated in the configuration, if not the
# fields will contain a '-') are:
# $2 count
# $3 source IP
# $4 destination IP
# $5 protocol
# $6 source port
# $7 destiniation port

# Debianized by Alberto Gonzalez Iniesta <agi@agi.as> Sun, 11 Nov 2001
# Source configuration generated from debconf's values
. /etc/default/fwlogwatch

IPCHAINS=/sbin/ipchains
IPTABLES=/sbin/iptables
LOG=/var/log/messages	#location of monitored log
TARGET=fwlw		#ipchains/iptables name of chain that fwlogwatch will use
IP=$3		#had to convert input parameter to a script variable for use in awk
RETVAL=0
NOW=$(date +'%b %_d %Y %H:%M:%S')

TRIGGER=""		
# number of counts ($2) that will trigger a 'response' -- by default this is same as the count that triggers 'alert' 
#  but I wanted alerts and responses to be different.  Set the alert trigger level in /etc/fwlogwatch.config.  
#  If you want to use the same trigger level then set TRIGGER=""

#translate common protocol numbers into human readable
case "$5" in
	1)
		PROTO=ICMP ;;

	6)
		PROTO=TCP ;;

	17)
		PROTO=UDP ;;

	*)	PROTO="-" ;;

esac



case "$1" in
##############################################################################

start)
  case "$MODE" in
  ipchains)
    if $IPCHAINS -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPCHAINS -F $TARGET
    else
      $IPCHAINS -N $TARGET
      $IPCHAINS -I input -j $TARGET
    fi
  ;;
  iptables)
    if $IPTABLES -t filter -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPTABLES -F $TARGET
    else
      $IPTABLES -N $TARGET
      $IPTABLES -I INPUT -j $TARGET
      $IPTABLES -I FORWARD -j $TARGET
    fi
  ;;
  # Insert setup for custom response here
  *)
    RETVAL=1
  ;;
  esac
;;

##############################################################################

add)
	if [[ -z "$3" ]]; then	#if no ip no. then exit with error
    	exit 1
	fi
	if [[ $2 -lt $TRIGGER ]]; then	#continue only if count of ip connections meets the level set above in $TRIGGER
		exit 0
	fi

  case "$MODE" in
  ipchains)
    if $IPCHAINS -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPCHAINS -A $TARGET -s $3 -j DENY
    else
      $IPCHAINS -N $TARGET
      $IPCHAINS -I input -j $TARGET
      $IPCHAINS -A $TARGET -s $3 -j DENY
    fi
  ;;
  iptables)
    if $IPTABLES -t filter -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPTABLES -A $TARGET -s $3 -j DROP
    else
      $IPTABLES -N $TARGET
      $IPTABLES -I INPUT -j $TARGET
      $IPTABLES -I FORWARD -j $TARGET
      $IPTABLES -A $TARGET -s $3 -j DROP
    fi

####### added by LJM 11/20/2010 to email after a fwlogwatch response, taken mostly from fwlw_notify script

### Put these logging commands before the log parsing so that the most recent entries will show in logs
# Use the following line to generate a custom log entry:
	LOGENTRY="RESPONSE: $3 has been banned for $2 failed attempts"
#log to syslog:
	/usr/bin/logger -p daemon.alert -t "fwlogwatch" $LOGENTRY
# log action to a custom log file:
	/bin/echo -e "$NOW $HOSTNAME fwlogwatch: $LOGENTRY" >> /var/log/fwlogwatch.log


# If there's an EMAIL address, we'll send alerts to it
	if [[ "$EMAIL" != "" ]]; then
	
# $RECENT looks at each line of log file, selects only lines with the source ip in fld 10 and fld 7 begins with Shorewall, then 
# NOTE: the next 3 variables search all the log files, even the gzipped rotated files
#  Scans each line to find which fld has the PROTO= info.  Then prints the desired info and sorts by the date fields 
#  (because the searched logs are not necessarily in date order due to log rotations). Prints last 24 entries.
# NOTE: Since the date in logs does not have a year field the sort below cannot sort by year and therefore
#  gives confusing results particularly in the early days of a new year (Dec of the previous year 
#  appears AFTER Jan of current year).
		RECENT=$(zcat -f $LOG* | awk -v IP=$IP '$10=="SRC="IP && $7 ~/^Shorewall/ {for (i=NF;i>0;i--) {if ($i ~/^PROTO=/) {printf "%-4s%-3s%8s  %s  %s\n",$1,$2,$3,$i,$(i+2)}}} ; $7==IP && (/banned/ || /unbanned/) {printf "%-4s%-3s%8s  %s\n",$1,$2,$3,$10}' | sort -s -k1,1M -k2,2n -k3.1b,3.2bn -k3.4b,3.5bn -k3.7b,3.8bn | tail -n24)

		PREVLOGGED=$(zcat -f $LOG* | awk -v IP=$IP '$5=="fwlogwatch:" && $7==IP && $10 ~/unbanned/ {print $7}' | uniq)

		BANNED=$(zcat -f $LOG* | awk -v IP=$IP '($7==IP && $0 ~/ been banned /) { n++ }; END { print n+0 }')


	# Prepare email text and subject -- subject is also used for custom log file entry:
	
		MSG1="$HOSTNAME fwlogwatch RESPONSE: $2 failed attempts from source $3 on $NOW\n\n"
		MSG2="This source has been temporarily banned by the $HOSTNAME firewall. The ${MODE} firewall rule for this ip address is: \n\
$($IPTABLES -S $TARGET | grep $3) \n\n"
		if [[  -z "$PREVLOGGED" ]] ; then
			MSG3=""
		else
			MSG3="PREVIOUSLY BANNED.  This source has been banned $[ BANNED-1 ] previous times in recent logs.\n\
If you wish to blacklist it manually you can use this suggested Shorewall blacklist file entry:\n\
$3\t$PROTO\t$7\t# refer to fwlogwatch RESPONSE on $NOW\n\n"
		fi
		MSG4="If desired you can unban this ip at any time with command:  /etc/fwlogwatch/fwlw_respond remove - $3\n\n"
		MSG5="You can view ALL the fwlogwatch generated rules with command:  $IPTABLES -S $TARGET    or    /sbin/shorewall show $TARGET \n\n"
		MSG6="Here is a summary of the most recent attempts originating from ip address $3,\n\
you can see the full log entries with command   zgrep $3 $LOG*\n$RECENT\n\n"
		MSG7="Here is Whois info for $3 : \n$(whois $3 | sed "/^[#%]/d")" 
		SUBJ="RESPONSE: $3 to port $6, $PROTO"

		#send the email:
		/bin/echo -e "$MSG1$MSG2$MSG3$MSG4$MSG5$MSG6$MSG7" | /usr/bin/mail -s "$HOSTNAME fwlogwatch $SUBJ" $EMAIL
	fi


#######	

 ;;
  # Insert custom response action here
  *)
    RETVAL=1
  ;;
  esac
;;

##############################################################################

remove)
  if [[ -z "$3" ]]
  then
    exit 1
  fi
	### Use the following line to generate a custom log entry
    LOGENTRY="RESPONSE: $3 has been unbanned"
	/usr/bin/logger -p daemon.alert -t "fwlogwatch" $LOGENTRY
    /bin/echo -e "$NOW $HOSTNAME fwlogwatch: $LOGENTRY" >> /var/log/fwlogwatch.log

  case "$MODE" in
  ipchains)
    if $IPCHAINS -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPCHAINS -D $TARGET -s $3 -j DENY
    else
      RETVAL=1
    fi
  ;;
  iptables)
	if [[ "$3" = ALL ]]; then		#if $3 = ALL then (flush the fwlw chain completely / reset it to nothing dropped)
		$IPTABLES -F fwlw >/dev/null
	else
    	if $IPTABLES -t filter -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
	    then
	      $IPTABLES -D $TARGET -s $3 -j DROP
	    else
	      RETVAL=1
	    fi
	fi
  ;;
  # Insert custom response action stop here
  *)
    RETVAL=1
  ;;
  esac
;;

##############################################################################

stop)
  case "$MODE" in
  ipchains)
    if $IPCHAINS -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPCHAINS -F $TARGET
      $IPCHAINS -D input -j $TARGET
      $IPCHAINS -X $TARGET
    fi
  ;;
  iptables)
    if $IPTABLES -t filter -n -L $TARGET 2>/dev/null | /bin/grep "Chain $TARGET " >/dev/null
    then
      $IPTABLES -F $TARGET
      $IPTABLES -D INPUT -j $TARGET
      $IPTABLES -D FORWARD -j $TARGET
      $IPTABLES -X $TARGET
    fi
  ;;
  # Insert cleanup for custom response here
  *)
    RETVAL=1
  ;;
  esac
;;

##############################################################################

*)
  echo "Usage: $0 {start|add|remove|stop} [count src_ip dst_ip protocol src_port dst_port]"
;;

##############################################################################
esac
exit $RETVAL
# EOF
